<?php
/**
 * 商户信息
 * User: ZHIYUAN
 * Date: 2019-08-05
 * Time: 16:33
 */
namespace YsepaySdk\Merchant;

use YsepaySdk\Kernel\BaseClient;
use YsepaySdk\Kernel\ResponseException;
use YsepaySdk\Kernel\YsepayException;

class Client extends BaseClient
{
    /**
     * 商户余额查询
     * @param $data
     * @return \YsepaySdk\BasicService\ResponseInterface
     * @author tu6ge
     */
    
    public function getBalance($merchant_usercode){
        $myParams = [];
        $myParams['method'] = 'ysepay.merchant.balance.query';

        $biz_content_arr = array(
            "merchant_usercode" => $merchant_usercode,
        );
        $myParams['biz_content'] = \GuzzleHttp\json_encode($biz_content_arr, JSON_UNESCAPED_UNICODE);//构造字符串
        return $this->app->basic->httpPost( $this->api_urls['commonapi_url'], $myParams, 'ysepay_merchant_balance_query_response');
    }

    /**
     * 待结算户到一般户/银行卡（实时）
     *
     * @param [type] $data
     * @return void
     */
    public function withdraw_d0($data){
        $myParams=[];
        $myParams['method']='ysepay.merchant.withdraw.d0.accept';
        $biz_content_arr=[
            'out_trade_no'=>$data['out_trade_no'],
            'currency'=>'CNY',
            'total_amount'=>$data['total_amount'],
            'subject'=>$data['subject'],
            'shopdate'=>date('Ymd')
        ];
        if(isset($data['merchant_usercode'])){
            $biz_content_arr['merchant_usercode']=$data['merchant_usercode'];//商户号,1.当商户号为空时，默认将partner_id作为商户号
        }
        if(isset($data['card_type'])){
            $biz_content_arr['card_type']=$data['card_type'];//到账卡类型 1-提现卡，空默认为结算卡
            if($data['card_type']==1){
                $biz_content_arr['bank_account_no']=$data['bank_account_no'];
            }
        }

        $myParams['biz_content'] = \GuzzleHttp\json_encode($biz_content_arr, JSON_UNESCAPED_UNICODE);//构造字符串
        return $this->app->basic->httpPost( $this->api_urls['commonapi_url'], $myParams, 'ysepay_merchant_withdraw_d0_accept_response');
    }

    /**
     * 实时提现交易(一般户到银行卡)
     *
     * @param [type] $data
     * @return void
     */
    public function withdraw_quick($data){
        $myParams=[];
        $myParams['method']='ysepay.merchant.withdraw.quick.accept';
        $biz_content_arr=[
            'out_trade_no'=>$data['out_trade_no'],
            'currency'=>'CNY',
            'total_amount'=>$data['total_amount'],
            'subject'=>$data['subject'],
            'shopdate'=>date('Ymd'),
            'bank_account_no'=>$data['bank_account_no'],
        ];
        if(isset($data['merchant_usercode'])){
            $biz_content_arr['merchant_usercode']=$data['merchant_usercode'];
        }
        if(isset($data['card_type'])){
            $biz_content_arr['card_type']=$data['card_type'];//到账卡类型 1-提现卡 2-结算卡，为空则默认1-提现卡
        }
        $myParams['biz_content'] = \GuzzleHttp\json_encode($biz_content_arr, JSON_UNESCAPED_UNICODE);//构造字符串
        return $this->app->basic->httpPost( $this->api_urls['commonapi_url'], $myParams, 'ysepay_merchant_withdraw_quick_accept_response');
    }

    /**
     * 商户提现查询
     *
     * @return void
     */
    public function withdrawQuery($out_trade_no){
        $myParams=[];
        $myParams['method']='ysepay.merchant.withdraw.quick.query';
        $biz_content_arr=[
            'out_trade_no'=>$out_trade_no,
            'shopdate'=>date('Ymd')
        ];
        $myParams['biz_content'] = \GuzzleHttp\json_encode($biz_content_arr, JSON_UNESCAPED_UNICODE);//构造字符串
        return $this->app->basic->httpPost( $this->api_urls['commonapi_url'], $myParams, 'ysepay_merchant_withdraw_quick_query_response');
    }



    
}