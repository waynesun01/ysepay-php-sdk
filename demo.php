<?php
require_once "vendor/autoload.php";
$config = [
    'business_gate_cert'    => './sm2businessgate.cer',    //公钥路径
    'private_cert'          => './aa.sm2',           //私钥路径
    'partner_id'            => 'aa', //商户号
    'seller_id'             => '',
    'seller_name'           => '',

    'pfxpassword'           => '',
    'merchant_code'         => '',

    'business_code'         => '',
    'log'   => [
        'path' => __DIR__ . '/info.log',
        'name'  => 'ysepay',
    ],
    'sign_type'       => 'RSA',
    'appid'            => '公众号appid',
    'weapp_appid' => '小程序appid'
];
$app = new \YsepaySdk\Client($config);
$notify_url='127.0.0.1';
$token=$app->wxpay->getToken(['notify_url'=>$notify_url]);
var_dump($token);
