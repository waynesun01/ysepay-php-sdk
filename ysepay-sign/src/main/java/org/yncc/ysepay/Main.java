package org.yncc.ysepay;

import org.yncc.ysepay.utils.YsPaySignUtils;

import java.util.Map;

public class Main {

    public static String sign(String sm2File,Map<String,String>data) throws Exception{
        try{
            System.out.println("sm2 file:"+sm2File);
            String sign=YsPaySignUtils.sign(sm2File,data);
            return sign;
        }catch (Exception e){
            System.out.println("sign err:" + e.getMessage());
            throw new Exception(e.getMessage());
        }
    }
}
